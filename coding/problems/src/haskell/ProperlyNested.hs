-- \$ runhaskell ProperlyNested.hs "{()}"
module ProperlyNested where

import Data.List (uncons)
import Data.Map (Map, elems, fromList, (!))
import Data.Maybe (fromJust)
import Data.Tuple
import System.Environment (getArgs)

chars :: Map Char Char
chars =
  fromList $
    fmap -- we swap because we use a right fold to take benefit of laziness
      swap
      [ ('(', ')'),
        ('[', ']'),
        ('{', '}')
      ]

properlyNested :: String -> Int
properlyNested = result . null . foldr go []
  where
    go _ ('e' : _) = "e" -- not needed, but exits early on failure
    go s acc
      | s `elem` chars =
          let (a, aTail) = fromJust $ uncons acc
           in if null acc || a /= s then "e" else aTail
    go s acc = (chars ! s) : acc
    result r = if r then 1 else 0

main :: IO ()
main = do
  string : _ <- getArgs
  print $ properlyNested string
