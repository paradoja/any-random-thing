{
  description = "Random things Ruby";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let 
          inherit (nixpkgs.lib) optional optionals;

          rubyVersion = pkgs.ruby_3_1;
          ruby = rubyVersion.withPackages (ps: with ps;
          [
            solargraph
            pry
          ]);

        in pkgs.mkShell {
          buildInputs = with pkgs; 
          [ ruby
            bundler
            bundix

            nixfmt
            yaml-language-server
          ];
          shellHook = "";
        };
      });
}
