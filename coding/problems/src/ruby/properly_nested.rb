#!/usr/bin/env ruby

CHARS = { '(' => ')', '[' => ']', '{' => '}' }.freeze

def properly_nested(str)
  str.each_char.inject([]) do |acc, current|
    if CHARS.values.member?(current)
      !acc.nil? && acc.first == current && acc.drop(1) || ['e']
    else
      acc.unshift(CHARS[current])
    end
  end.empty? && 1 || 0
end

puts properly_nested(ARGV.first)
